#!/bin/sh

set -e

echo "Current commit SHA:"
echo $CI_COMMIT_SHA

cd /builds/$CI_PROJECT_PATH
git log --oneline -n 1

ls -a
cat index.html

cp index.html /usr/share/nginx/html

exec "$@"